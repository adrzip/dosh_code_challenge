package com.adrian.adrianhernandez_androidcodechallenge

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.AccelerateInterpolator


/**
 * Created by Adrian on 6/14/2017.
 */
class MainActivityFragment : LifecycleFragment(), SubredditAdapter.Listener {

    lateinit var recyclerSubreddits: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    var isLoading = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val tView = inflater.inflate(R.layout.fragment_main, container, false)

        recyclerSubreddits = tView.findViewById(R.id.recycler_subreddits) as RecyclerView
        swipeRefreshLayout = tView.findViewById(R.id.swipe_refresh_layout) as SwipeRefreshLayout

        recyclerSubreddits.layoutManager = LinearLayoutManager(activity)
        recyclerSubreddits.setHasFixedSize(false)
        swipeRefreshLayout.setOnRefreshListener {
            loadReddits((activity as MainActivity).getSearchTerm())
        }
        loadReddits((activity as MainActivity).getSearchTerm())
        return tView
    }

    fun loadReddits(searchTerm: String) {
        if(!isLoading) {
            isLoading = true
            clearReddits()
            viewModel.getSubreddits(activity, searchTerm).observe(this@MainActivityFragment, Observer {
                if(it != null) {
                    recyclerSubreddits.adapter = SubredditAdapter(activity, it, this@MainActivityFragment)
                    recyclerSubreddits.animate().alpha(1.0f).setDuration(440).setInterpolator(AccelerateInterpolator()).start()
                    swipeRefreshLayout.isRefreshing = false
                    isLoading = false
                }
            })
        }
    }

    fun clearReddits() {
        recyclerSubreddits.adapter = null
        recyclerSubreddits.animate().alpha(0.0f).setDuration(260).setInterpolator(AccelerateInterpolator()).start()
    }

    override fun onSubredditSelected(subreddit: Subreddit) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        var title = subreddit.title
        var url = subreddit.url
        var text = "Check out this super cool post I found on Reddit\n\n$title\n$url"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(Intent.createChooser(intent, "Send Email"))
    }
}
