package com.adrian.adrianhernandez_androidcodechallenge

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.LiveData
import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import java.net.URLEncoder


/**
 * Created by Adrian on 6/14/2017.
 */
class MainViewModel: ViewModel() {

    companion object {
        val DEFAULT_SUBREDDIT = "funny"
        val TAG = MainViewModel::class.simpleName
    }

    var subreddits = MutableLiveData<List<Subreddit>>()

    fun getSubreddits(context: Context, category: String): LiveData<List<Subreddit>> {
        loadSubreddits(context, category)
        return subreddits
    }

    private fun loadSubreddits(context: Context, category: String) {
        var queue = Volley.newRequestQueue(context)
        var encodedCategory = URLEncoder.encode(category, "UTF-8")
        val url = "https://www.reddit.com/r/$encodedCategory/.json"
        val headers = HashMap<String, String>()
        var req = GsonRequest<SubredditResponse>(url, SubredditResponse::class.java, headers, Response.Listener<SubredditResponse> {
            if(it != null) {
                val newSubreddits = mutableListOf<Subreddit>()
                it.data.children.forEach { newSubreddits.add(it.data) }
                subreddits.value = newSubreddits

            }
        }, Response.ErrorListener {
            Log.e(TAG, "")
        })
        queue.add(req)
    }
}