package com.adrian.adrianhernandez_androidcodechallenge

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.Menu

class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG = MainActivity::class.simpleName
    }

    lateinit var toolbar: Toolbar

    private var searchTerm: String = MainViewModel.DEFAULT_SUBREDDIT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        if(intent != null && !intent.hasExtra(SearchManager.QUERY)) {
            intent.putExtra(SearchManager.QUERY, searchTerm)
        }
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        if(intent != null) handleIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setQuery(searchTerm, true)
        searchView.setIconifiedByDefault(false)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if(query != null) searchTerm = query
                val frag = supportFragmentManager.findFragmentById(R.id.fragment) as MainActivityFragment
                frag.loadReddits(searchTerm)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText != null && newText.isBlank()) {
                    val frag = supportFragmentManager.findFragmentById(R.id.fragment) as MainActivityFragment
                    frag.clearReddits()
                    return true
                }
                return false
            }

        })
        return true
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH.equals(intent.action)) {
            searchTerm = intent.getStringExtra(SearchManager.QUERY)
        }
    }

    fun getSearchTerm(): String {
        return searchTerm
    }
}
