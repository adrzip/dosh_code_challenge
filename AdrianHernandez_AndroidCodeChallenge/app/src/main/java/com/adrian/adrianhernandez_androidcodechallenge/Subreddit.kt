package com.adrian.adrianhernandez_androidcodechallenge

/**
 * Created by Adrian on 6/14/2017.
 */
data class Subreddit (var id: String, var title: String, var num_comments: Int, var ups: Int,
                      var downs: Int, var thumbnail: String, var author: String, var url: String)