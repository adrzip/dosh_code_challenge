package com.adrian.adrianhernandez_androidcodechallenge

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.bindView
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by Adrian on 6/14/2017.
 */
class SubredditAdapter: RecyclerView.Adapter<SubredditAdapter.SubredditViewHolder> {

    companion object {
        private val TAG = SubredditAdapter::class.simpleName
        private val TRANSLATION_X = 50.0f
    }

    private var context: Context
    private var layoutInflater: LayoutInflater
    private var picasso: Picasso
    private var subreddits: List<Subreddit>
    private var listener: Listener
    private var custom_font: Typeface

    constructor(context: Context, subreddits: List<Subreddit>, listener: Listener) {
        this.context = context
        layoutInflater = LayoutInflater.from(context)
        picasso = Picasso.with(context)
        this.subreddits = subreddits
        this.listener = listener
        custom_font = Typeface.createFromAsset(context.assets,  "bebasneue.ttf")
    }

    override fun onBindViewHolder(holder: SubredditViewHolder?, position: Int) {
        if (holder == null) return
        val subreddit = subreddits[position]
        if(subreddit.author != null) {
            holder.author.typeface = custom_font
            holder.author.text = subreddit.author
        }
        if(subreddit.title!= null) {
            holder.title.text = subreddit.title
        }
        if(!subreddit.thumbnail.isNullOrBlank()) {
            Picasso.with(context).load(subreddit.thumbnail).into(holder.thumb)
            holder.thumb.visibility = View.VISIBLE
        } else {
            holder.thumb.visibility = View.INVISIBLE
        }
        if(subreddit.num_comments != null) {
            holder.comments.text = "" + subreddit.num_comments + " Comments"
        }
        if(subreddit.ups!= null) {
            holder.ups.text = ""+subreddit.ups+" ups"
        }
        if(subreddit.downs!= null) {
            holder.downs.text = ""+subreddit.downs+" downs"
        }

        holder.itemView.setOnClickListener({
            listener.onSubredditSelected(subreddit)
        })

        holder.root.alpha = 0.0f
        holder.root.translationX = TRANSLATION_X
        holder.root.animate().alpha(1.0f).translationX(-TRANSLATION_X).setDuration(440).
                setInterpolator(DecelerateInterpolator()).start()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) = SubredditViewHolder(layoutInflater.inflate(R.layout.item_reddit, parent, false))


    override fun getItemCount() = subreddits.size

    open class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)

    class SubredditViewHolder : ViewHolder {
        val root: ViewGroup by bindView(R.id.item_root)
        val author: TextView by bindView(R.id.text_author)
        val title: TextView by bindView(R.id.text_title)
        val comments: TextView by bindView(R.id.text_comments)
        val ups: TextView by bindView(R.id.text_ups)
        val downs: TextView by bindView(R.id.text_downs)
        val thumb: CircleImageView by bindView(R.id.image_thumb)

        constructor(itemView: View?) : super(itemView) {
            if(itemView != null) ButterKnife.bind(this, itemView)
        }
    }

    interface Listener {
        fun onSubredditSelected(subreddit: Subreddit)
    }
}
