package com.adrian.adrianhernandez_androidcodechallenge

/**
 * Created by Adrian on 6/14/2017.
 */
data class SubredditResponse (val data: SubredditResponseData)
data class SubredditResponseData (val children: List<Child>)
data class Child (val data: Subreddit)